Use Guide
=========

Here you will learn how to use the package correctly and you will see useful examples

.. toctree::
   :maxdepth: 2

   constants/index
   utils/index