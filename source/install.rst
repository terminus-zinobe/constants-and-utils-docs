Installation
============

Install Constants and utils is pretty simple. Here is a step by step plan to how do it.


You have to install the package in the virtual environment::

    pip install constants_and_utils

If you need use MongoEngine functionalities::

    pip install constants_and_utils[mongoengine]
