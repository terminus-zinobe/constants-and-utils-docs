Constants
=========

This package provides generic constants and enum classes

.. toctree::
   :maxdepth: 2

   enums
   responses