Enums
-----

Enums classes


HttpCode
~~~~~~~~

Enum class HttpCode

::

    >>> from constants_and_utils.constants.enums import HttpCode
    >>> HttpCode.OK.value
    200


Gender
~~~~~~

Enum class Gender

::

    >>> from constants_and_utils.constants.enums import Gender
    >>> Gender.FEMALE.value
    'F'
