Welcome to Contants-And-Util's documentation!
====================================================

Constants and utils is package with constants and generic utilities to implement in python projects

Get the code
------------

The `source <https://gitlab.com/terminus-zinobe/constants-and-utils>`_ is available on GitLab. 

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:

    install
    userguide



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
