Mongo Engine
------------


Diff Mongo Objects
~~~~~~~~~~~~~~~~~~

.. note ::

    It require to install *mongoengine*::

        pip install constants_and_utils[mongoengine]


Return diff between two objects of Mongo Document type::

    >>> import models
    >>> from constants_and_utils.utils.mongoengine import diff_mongo_objects
    >>> p1 = models.Product(
    ...     uuid='abc-123',
    ...     name='aproduct',
    ...     version=1,
    ...     company=models.Company(
    ...         name='company-name',
    ...     )
    ... )
    >>> p2 = models.Product(
    ...     uuid='abc-123',
    ...     name='aproduct',
    ...     version=2,
    ...     company=models.Company(
    ...         name='company-name2',
    ...     )
    ... )
    >>> diff_mongo_objects(
    ...     old_object=p1,
    ...     new_object=p2,
    ... )
    {
        'values_changed':{
            "root['company']['name']":{
                'new_value':'company-name2',
                'old_value':'company-name'
            },
            "root['version']":{
                'new_value':2,
                'old_value':1
            }
        }
    }
