Use Cases
---------

Use cases classes


Response
~~~~~~~~

Response object for use cases.

::

    >>> from constants_and_utils.utils.use_cases import Response
    >>> r = Response(http_code=200, message='OK')
    >>> r.message
    'OK'


ExportResponse
~~~~~~~~~~~~~~

Response for use cases export file.

::

    >>> from constants_and_utils.utils.use_cases import ExportResponse
    >>> r = ExportResponse(http_code=200, message='OK', file_name='file.csv')
    >>> r.file_name
    'file.csv'


UseCaseInterface
~~~~~~~~~~~~~~~~

Contract for use cases.

::

    >>> from constants_and_utils.utils.use_cases import UseCaseInterface
    >>> class UseCase(UseCaseInterface):
    ...     pass


DataResponse
~~~~~~~~~~~~

Response for use cases.

::

    >>> from constants_and_utils.utils.use_cases import DataResponse
    >>> r = DataResponse(http_code=200, message='OK', data={})
    >>> r.data
    {}
