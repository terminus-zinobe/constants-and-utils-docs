Helpers
-------

Helpers functions


recursive_get
~~~~~~~~~~~~~

Apply a get recursive to the dictionary.

::

    >>> from constants_and_utils.utils.helpers import recursive_get
    >>> recursive_get({'NUMBER': {'ONE': 1 }}, 'NUMBER.ONE')
    1


kebab_case
~~~~~~~~~~

Convert string into kebab case or slug.

::

    >>> from constants_and_utils.utils.helpers import kebab_case
    >>> kebab_case('Hello world')
    'hello-world'


camel_to_snake
~~~~~~~~~~~~~~

Convert a string from camel case to snake case.

::

    >>> from constants_and_utils.utils.helpers import camel_to_snake
    >>> camel_to_snake('HelloWorld')
    'hello_world'


dict_keys_to_snake
~~~~~~~~~~~~~~~~~~

Convert the keys from camel case dictionary to snake case.

::

    >>> from constants_and_utils.utils.helpers import dict_keys_to_snake
    >>> d = {'HelloWorld': 'message'}
    >>> dict_keys_to_snake(d)
    {'hello_world': 'message'}


setter_object_attrs
~~~~~~~~~~~~~~~~~~~

Add specific value keys to

::

    >>> from constants_and_utils.utils.helpers import setter_object_attrs
    >>> class AnyClass:
    ...     any_attribute = "Any"
    ...
    >>> any_object = AnyClass()
    >>> any_dict = {'some': 'value', 'another': 'value'}
    >>> any_list = ['some']
    >>> setter_object_attrs(any_object, any_list, any_dict)
    >>> print(any_object.some)
    'value'
