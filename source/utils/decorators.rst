Decorators
----------

Decorators functions.


result_to_json
~~~~~~~~~~~~~~

Convert result into json from a function.

::

    >>> from constants_and_utils.utils.decorators import result_to_json
    >>> @result_to_json
    ... def hello():
    ...     return {'message': 'hi'}


debugger_handle
~~~~~~~~~~~~~~~

Use debugger_handle decorator to show received data and response data.

::

    >>> from constants_and_utils.utils.decorators import debugger_handle
    >>> 
    >>> class UseCase():
    ...     @debugger_handle
    ...     def handle():
    ...         return {'message': 'hi'}
