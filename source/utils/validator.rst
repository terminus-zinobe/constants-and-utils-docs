Validator
~~~~~~~~~

Validation structures

Next fields are available for verification as show in the next examples

- no_spaces
- mongo_id
- only_numbers
- multiple

Examples
~~~~~~~~

Generic example for each use case

::

    >>> from constants_and_utils.utils.validator  import Validator
    >>> schema = {'name': {'type': 'string', 'no_spaces': True}}
    >>> v = Validator(schema)
    >>> body = {'name': 'Juan'}
    >>> v.validate(body)
    True
